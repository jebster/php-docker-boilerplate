<?php
require 'vendor/autoload.php';
use \Curl\Curl;


$curl = new Curl();
$curl->get('https://ifconfig.co/json');

if ($curl->error) {
    echo 'Error: ' . $curl->errorCode . ': ' . $curl->errorMessage . "\n";
} else {
    echo 'City: ' . $curl->response->city;
}